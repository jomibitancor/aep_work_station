import requests

class Database:
    def upload(self, station_number, data):
        '''
        Definition:
            Uploads to the API for the API to append to database

        Parameters:
            station_number (str) - station_number
            data (dict) - data with the corresponding station_number

        Returns:
            true or none (bool) - for main code to determine if the API properly received the data or not

        '''
        payload = {"station_number": station_number, "data_bulk": data}
        r = requests.post("http://api.micro-sensors.com/post", json=payload)
        # r = requests.post("http://127.0.0.1:3000/post", json=payload) # ! Uncomment for local testing

        if r.status_code == 200:
            return True
        else:
            return None

    def upload_many(self, station_number, data_many):
        '''
        Definition:
            Uploads to he API for the API to append to database. 

            Parameters:
                station_number (str) - station_number
                data_many (list) - a list of lists that contains the data
        '''
        fixed_header = ['year', 'month', 'day', 'hour', 'minute', 'second', 'pt25', 'ptTVOC', 'ptHCHO', 'ptCO2', 'ptTemp', 'ptHum', 'nv25', 'nv10', 'sh180s', 'sh10s', 'sh30s', 'sh60s', 'o3', 'pcbTemp', 'pcbHum', 'inletTemp', 'inlethum', 'temperature', 'humidity', 'inputcurr', 'fancurrent', 'fanrpm', 'windspeed', 'winddirection', 'inputvoltage', 'lat', 'lon', 'battery']
        mass_upload = []

        for data_bulk in data_many:
            mass_upload.append( {"station_number": station_number, "data_bulk": dict(zip(fixed_header, data_bulk))} )

        payload = {"mass_upload": mass_upload}
        r = requests.post("http://api.micro-sensors.com/postMany", json=payload)
        # r = requests.post("http://127.0.0.1:3000/postMany", json=payload)  # ! Uncomment for local testing

        if r.status_code == 200:
            return '200'
        elif r.status_code == 413:
            return '413'
        else:
            return None        