import time
import requests
import sys
import os
import csv
import serial
import json
import _thread
from database import Database

db = Database()

def get_csv_files(csv_path):
    '''
        Gets the csv files in the provided directory
    '''
    csv_files = []
    try:
        
        if csv_path:
            for file in os.listdir(csv_path):
                if file.endswith(".csv") or file.endswith(".CSV"):
                    csv_files.append(file)
    except:
        print("No CSV path provided...")
        print("Exiting in 5 seconds...")
        time.sleep(5)
        exit()

    return csv_files

def get_headers(csv_file, csv_path):
    '''
        Gets the headers of the OUTPUT csv_file, along witht the provided csv_path for Python to open the file

        Parameters:
            csv_file (str) - csv file at the workstation
            csv_path (str) - path of the csv file of the workstation

        Returns:
            headers - a list that contains all the headers of the csv file
    '''
    csv_path_complete = csv_path + "\\" + csv_file
    with open(csv_path_complete) as f: 
        reader = csv.DictReader(f)
        headers = reader.fieldnames
    return headers

def check_csv_files(csv_files, station_number):
    '''
        Checks the csv files list if the station number (i.e. data received) is in the csv files

        Parameters:
            csv_files (list) - csv file at the workstation
            station_number (str) - string of the received arduino data that has the microstation info
        Returns:
            file (str) - returns the string of the file that was found from csv_files
    '''
    file_flag = None
    for file in csv_files:
        if station_number == file[0:-4]:
            file_flag = file
    
    return file_flag

def parse_data(data_read,csv_output_path,csv_files):
    '''
        A separate thread that parses the string that was received from the serial monitor of the basestation. 

        Parameters:
            data_read (str) - string of the data read from basestation
            csv_output_path (str) - string path of the output folder selected
            csv_files (list) - list of CSV files that were detected from the output folder
        Returns:
            No returns
    '''
    header = "year,month,day,hour,minute,second,pt25,ptTVOC,ptHCHO,ptCO2,ptTemp,ptHum,nv25,nv10,sh180s,sh10s,sh30s,sh60s,o3,pcbTemp,pcbHum,inletTemp,inlethum,temperature,humidity,inputcurr,fancurrent,fanrpm,windspeed,winddirection,inputvoltage,lat,lon,battery"
    header_list = header.split(',')

    comma_split = data_read.split(',')
    station_number = comma_split.pop(0)[-4:]

    last_entry = comma_split.pop(-1)
    comma_split.append(last_entry.split('-')[0])

    json_dict = dict(zip(header_list,comma_split))
    json_data = json.loads(json.dumps(json_dict))

    csv_file = check_csv_files(csv_files, station_number)
    if csv_file:
        headers = get_headers(csv_file, csv_output_path)
        with open(csv_output_path + "//" + csv_file, 'a', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writerow(json_data) # Insert received data into output folder 
            print("[OK] Appended to local DB...")

            if db.upload(station_number, json_data):
                print("[OK] Appended to online DB...")
            else:
                print("[ERR] Could not append to online DB...")
    else:
        print("[ERR] Could not append to local DB. Please check if there is a CSV file that matches the last 4 digits of the microstation number in the selected output folder")


if __name__ == "__main__":
    '''
        live.py uses sys.args library to take in the following:

        Parameters
            output path (str) - the path to the directory that contains the CSV files where the JSON data received will be appended to
            COM port (str) - port number of where the basestation is connected to. 
        Returns
            None
        Function
            To provide the user of the workstation the live feed of what the program is doing
    '''
    try:
        ser = serial.Serial(str(sys.argv[2]), baudrate=9600, timeout=1) # Initialize serial port communication
    except:
        print("Cannot connect to Arduino... Please check Device Manager and see if right COM port is provided")
        for i in range(0,5):            
            print("Exiting in {} seconds...".format(5-i))
            time.sleep(1)
        exit()

    try:
        csv_output_path = sys.argv[1]
        csv_files = get_csv_files(csv_output_path)

        while 1: 
            serial_monitor_read = str(ser.readline())
            data_read = ''
            if serial_monitor_read[2] == "\\" or len(serial_monitor_read) == 3:
                pass
            elif serial_monitor_read[2] == "?":
                data_read += str(serial_monitor_read)
                try:
                    _thread.start_new_thread(parse_data, (data_read,csv_output_path,csv_files,))
                except:
                    print("[ERR] Unable to start thread.")
            else:
                data_read += str(serial_monitor_read)
                print(data_read)
    except:
        print("[ERR] Something went wrong... Exiting in 5 seconds")
        time.sleep(5)
        exit()