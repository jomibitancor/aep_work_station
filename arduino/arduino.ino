#include <ArduinoJson.h>

int analogPin = 3;
int data = 0;
char userInput;
int i = 0;


void setup() {
  Serial.begin(9600);  
  
  
}

void loop() {
  const int capacity = JSON_OBJECT_SIZE(35);
  StaticJsonDocument<capacity> doc;
  
  if (Serial.available()>0){

    userInput = Serial.read();
    if(userInput == 'a'){
      doc["phone_number"] = "1_small_sample";
      doc["year"] = "19";
      doc["month"] = "6";
      doc["day"] = "5";
      doc["hour"] = "17";
      doc["minute"] = 48 + i;
      doc["second"] = "18";
      doc["pt25"] = "0";
      doc["ptTVOC"] = "0.12";
      doc["ptHCHO"] = "0";
      doc["ptCO2"] = "400";
      doc["ptTemp"] = "24.1";
      doc["ptHum"] = "21.5";
      doc["nv25"] = "0.8";
      doc["nv10"] = "1";
      doc["sh180s"] = "0";
      doc["sh10s"] = "0";
      doc["sh30s"] = "0";
      doc["sh60s"] = "0";
      doc["o3"] = "0";
      doc["pcbTemp"] = "23.1";
      doc["pcbHum"] = "23.7";
      doc["inletTemp"] = "23.1";
      doc["externalTemp"] = "25";
      doc["externalHum"] = "26";
      doc["inputCurr"] = "805";
      doc["fanCurr"] = "33";
      doc["fanRPM"] = "99";
      doc["windspeed"] = "99";
      doc["winddirection"] = "99";
      doc["inputVoltage"] = "11.9";
      doc["lat"] = "53.52322";
      doc["lon"] = "-113.526";
      doc["batt"] = "63";
      doc["RTCFlag"] = "99";
      doc["RTCtemp"] = "99";
      serializeJson(doc, Serial);
//      serializeJsonPretty(doc, Serial);
      i++;
      }
  }
}
