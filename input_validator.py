import re
import csv
import os
pattern_station_number = re.compile('(.*)>{3}')
pattern_new_calibration = re.compile('>{3}(.*)=')
pattern_equation = re.compile('=(.*)')

def validate_input(input, output_path):
    """
    This function validates the input of a calibration. It does this in multiple steps
        1. Check if the microstation exists in the current output folder

    Parameters:
        input (str) - input string to be validated
        output_path (str) - output folder path to ensure that 

    Returns:
        boolean - returns if the input to be validated is True or False

    """
    if len(input.strip())>0:
        try:
            station_number = pattern_station_number.match(input).group(1)
            new_calibration = pattern_new_calibration.search(input).group(1)
            whole_equation = pattern_equation.search(input).group(1)

            

            for file in os.listdir(output_path):
                if file.endswith(".csv") or file.endswith(".CSV"):
                    if station_number in file:  # If the typed in station number (for the calibration) is in the current output folder directory, then it's a valid station number
                        return True

        except:
            print('No match!')

    else:
        return False

    return ''

if __name__ == "__main__":

    input = '1_sample>>>tmp_cal='

    new_caliration = pattern_new_calibration.search(input).group(1)
    print(new_caliration)
    