from control import Controller
import sys

from PyQt5.QtWidgets import QMainWindow, QApplication


class App(QMainWindow):
    def __init__(self):
        self.app = QApplication(sys.argv)
        super().__init__()
        # self.resize(328, 275)
        self.resize(640,510)
        self.setWindowTitle('AEP Work Station Program')
        self.setupUi()

    def setupUi(self):
        control = Controller()
        self.setCentralWidget(control)
        self.show()

    def start(self):
        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            self.app.exec_()

if __name__ == "__main__":
    app = App()
    app.start()