from ui import Ui_Form
import os
import csv
import requests
from time import process_time
from PyQt5 import QtCore, QtGui, QtWidgets
import time
import input_validator
from database import Database
import json

db = Database()

class Controller(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.connect_signals()
        self.output_folder_path = ''
        self.com_port = ''
        self.input_folder_files = []
        

    def connect_signals(self):
        self.ui.button_ChooseFolder.clicked.connect(self.choose_input_folder)
        self.ui.button_ChooseOutputFolder.clicked.connect(self.choose_output_folder)
        self.ui.button_ChooseCOMPort.clicked.connect(self.choose_com_port)
        self.ui.button_Upload.clicked.connect(self.upload)
        self.ui.button_ChooseMode.clicked.connect(self.live_mode)
        self.ui.button_cal_Add.clicked.connect(self.calibrate_add)
        self.ui.button_cal_Edit.clicked.connect(self.calibrate_edit)
        self.ui.button_cal_Delete.clicked.connect(self.calibrate_delete)
        self.ui.button_cal_Clear.clicked.connect(self.calibrate_clear)
        self.ui.button_AddMicrostation.clicked.connect(self.add_microstation)
        self.ui.button_Upload.setEnabled(False)
        self.enable_disable_buttons(False)

    def enable_disable_buttons(self, signal):
        if signal:
            self.ui.button_cal_Add.setEnabled(True)
            self.ui.button_cal_Edit.setEnabled(True)
            self.ui.button_cal_Delete.setEnabled(True)
            self.ui.button_cal_Clear.setEnabled(True)
            self.ui.button_ChooseMode.setEnabled(True)
        else:
            self.ui.button_cal_Add.setEnabled(False)
            self.ui.button_cal_Edit.setEnabled(False)
            self.ui.button_cal_Delete.setEnabled(False)
            self.ui.button_cal_Clear.setEnabled(False)
            self.ui.button_ChooseMode.setEnabled(False)
        pass

    def add_microstation(self):
        new_microstation, done1 = QtWidgets.QInputDialog.getText(self, 'New Microstation', 'Enter last 4 digits of the microstation: ')
        if done1:
            try:
                fc = self.output_folder_path + "/" + new_microstation + ".CSV"
                f = open(fc, "w")
                f.write("year,month,day,hour,minute,second,pt25,ptTVOC,ptHCHO,ptCO2,ptTemp,ptHum,nv25,nv10,sh180s,sh10s,sh30s,sh60s,o3,pcbTemp,pcbHum,inletTemp,inlethum,temperature,humidity,inputcurr,fancurrent,fanrpm,windspeed,winddirection,inputvoltage,lat,lon,battery,\n")
                f.close()
                self.ui.textBrowser.setText("Successfully added a new .CSV file for new microstation: " + fc)
            except FileExistsError:
                print("File already exists")

    def choose_com_port(self):
        self.com_port, done1 = QtWidgets.QInputDialog.getText(self, 'COM Port', 'Enter: ')
        if done1:
            self.ui.textBrowser.setText("COM Port: " + self.com_port)
            return ''

    def choose_input_folder(self):
        '''
        Definition:
            Choose an input folder
        '''
        self.ui.listWidget.clear()
        self.input_folder_files = []

        path = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))
        if path:
            for file in os.listdir(path):
                if file.endswith(".csv") or file.endswith(".CSV"):
                    self.ui.listWidget.addItem(file)
                    self.input_folder_files.append(file)

        if self.ui.listWidget.count() > 0:
            self.ui.textBrowser.setText("Files ready to be uploaded")
            rel_path = os.path.relpath(path)
            cwd_path = os.path.dirname(__file__)
            if rel_path == '.':                
                self.input_folder_path = os.path.join(cwd_path)
            else:
                self.input_folder_path = os.path.join(cwd_path, rel_path)
            
            self.ui.button_Upload.setEnabled(True)

        else:
            self.ui.textBrowser.setText("No CSV files found! Please choose a different directory")
    
    def upload(self):
        '''
        Definition:
            This function should be able to upload a hotswapped SD card that contains data from the basestation.
        Assumptions:
            - The CSV files from the input will match the ones in the output folder path
                (ie. INPUT: 123_sample.csv -> OUTPUT: 123_sample.csv)
            - The CSV files from the input will have a fixed header of:
                [year,month,day,hour,minute,second,pt25,ptTVOC,ptHCHO,ptCO2,ptTemp,ptHum,nv25,nv10,sh180s,sh10s,sh30s,sh60s,o3,pcbTemp,pcbHum,inletTemp,externalTemp,externalHum,inputCurr,fanCurr,fanRPM,windspeed,winddirection,inputVoltage,lat,lon,batt,RTCFlag,RTCTemp]

            NOTE: This function DOES NOT organize but only appends the data from the input CSV into output. You can compensate for the shortcoming by simply opening the CSV file and sorting from there using Excel
        '''
        if self.ui.button_cal_Add.isEnabled() == True:
            for file in self.input_folder_files:
                to_be_appended = []

                # // Collect all data from input fle and prepare for appending to both local and online DB
                with open(self.input_folder_path + "//" + file, 'r') as input_file:
                    reader = csv.reader(input_file)
                    for data in reader:
                        to_be_appended.append(data)

                # // Append to local workstation CSV
                try:
                    with open(self.output_folder_path + "//" + file, 'a', newline='') as output_file:
                        writer = csv.writer(output_file)
                        for data in to_be_appended:
                            writer.writerow(data)
                except:
                    self.ui.textBrowser.setText("ERROR: The input <-> output filenames do not match")
                
                # // TODO: Append to database
                try:
                    upload_attempt = db.upload_many(file[0:-4], to_be_appended)
                    if upload_attempt == '200':
                        self.ui.textBrowser.setText("SUCCESS: Appended to online DB")
                    elif upload_attempt == '413':
                        self.ui.textBrowser.setText("ERROR: File too large for online DB. Data still appended to local DB")
                    else:
                        self.ui.textBrowser.setText("ERROR: Something went wrong uploading to online DB")
                except:
                    self.ui.textBrowser.setText("ERROR: Cannot upload to database.")
                
        else:
            self.ui.textBrowser.setText("ERROR: Please choose an output folder")
        pass

    def live_mode(self):
        try:
            try:
                os.system("start python live.py {} {}".format(self.output_folder_path, self.com_port))
            except:
                self.ui.textBrowser.setText("Please first select the OUTPUT FOLDER")    
            
        except:
            self.ui.textBrowser.setText("ERROR: Something went wrong when starting Live Mode")
        return ''

    def send_data(self, data_list):
        pass

    def choose_output_folder(self):
        """
        An assumption is made here that the output folder does contain the workstation DB.
        It won't work anyways if a calibration is added with a station number that doesn't exist from the output folder
        """
        self.output_folder_path = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))
        if self.output_folder_path:  # If an output folder is specified, enable the buttons
            self.ui.textBrowser.setText("Output Folder: " + str(self.output_folder_path))
            self.output_folder_chosen = True
            self.enable_disable_buttons(self.output_folder_chosen)
        pass

    def calibrate_add(self):
        calibration_entry, done1 = QtWidgets.QInputDialog.getText(self, 'Add Calibration', 'Add Calibration:')
        if done1:
            if input_validator.validate_input(calibration_entry, self.output_folder_path):  # Check if input is valid
                self.ui.listWidget_Calibrations.addItem(calibration_entry)
            else:
                self.ui.textBrowser.setText("Invalid calibration input. Please try again. Refer to the documentation if needed")
            
        return ''

    def calibrate_edit(self):
        print(self.ui.listWidget_Calibrations.currentItem().text())
        return ''

    def calibrate_delete(self):
        self.ui.listWidget_Calibrations.takeItem(self.ui.listWidget_Calibrations.currentRow())
        return ''

    def calibrate_clear(self):
        self.ui.listWidget_Calibrations.clear()
        
        return ''